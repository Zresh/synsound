package com.example.zresh.ms;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.zresh.ms.models.NPlaylist;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

public class PlaylistAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<NPlaylist> nPlaylists;
    private YoutubeActivity activity;
    public List<NPlaylist> filteredList;

    public PlaylistAdapter(YoutubeActivity a, List<NPlaylist> result) {
        filteredList = result;
        nPlaylists = result;
        activity = a;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return nPlaylists.size();
    }

    @Override
    public Object getItem(int i) {
        return nPlaylists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View viw = view;
        if (view == null) {
            viw = inflater.inflate(R.layout.player_item, null);
        }
        final NPlaylist nPlaylist = nPlaylists.get(i);
        final TextView vTitle = (TextView) viw.findViewById(R.id.pl_text);
        vTitle.setText(nPlaylist.getTitle());

        return viw;
    }
}
