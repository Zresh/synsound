package com.example.zresh.ms.models;

import com.google.api.services.youtube.model.Playlist;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.Call;
import retrofit2.http.POST;

import java.util.List;

public interface Link {
    @GET("/rooms")
    Call<List<Room>> getRoom();

    @POST("/rooms")
    Call<List<Room>> createRoom(@Body Room room);

    @POST("/playlist")
    Call<List<NPlaylist>> createNPlaylist(@Body NPlaylist nplaylist);

    @GET("/playlist")
    Call<List<NPlaylist>> getNPlaylist();
}
