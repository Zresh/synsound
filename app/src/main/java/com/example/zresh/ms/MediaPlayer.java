package com.example.zresh.ms;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.instacart.library.truetime.TrueTime;

import java.util.Date;

public class MediaPlayer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        new InitTrueTimeAsyncTask().execute();

        final TextView timeGMT = (TextView)findViewById(R.id.tt_time_gmt);
        final TextView timeDeviceTime = (TextView)findViewById(R.id.tt_time_device);
        final Button button = (Button)findViewById(R.id.tt_btn_refresh);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TrueTime.isInitialized()) {
                    new InitTrueTimeAsyncTask().execute();
                    return;
                }

                Date trueTime = TrueTime.now();
                Date deviceTime = new Date();
                deviceTime.getTime();
                timeGMT.setText(String.valueOf(trueTime.getTime()));
                //timePST.setText(String.valueOf(deviceTime.getTime()));
                timeDeviceTime.setText(String.valueOf(deviceTime.getTime()));
            }
        });
    }

}
