package com.example.zresh.ms;


public interface OnFileTaskCompleted {
    void onFileTaskCompleted(String url);
}
