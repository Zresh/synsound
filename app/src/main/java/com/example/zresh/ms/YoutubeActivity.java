package com.example.zresh.ms;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.zresh.ms.models.Link;
import com.example.zresh.ms.models.NPlaylist;
import com.google.api.services.youtube.model.SearchResult;

import java.nio.channels.AsynchronousFileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.zresh.ms.MainActivity.BASE_URL;

public class YoutubeActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, OnYoutubeTaskCompleted, AdapterView.OnItemClickListener {
    private ListView listView ;
    private SearchView searchView;
    private YouTubeAdapter adapter;
    public Object YoutubeResult;
    public ArrayList<com.example.zresh.ms.YoutubeResult> searchResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);
        listView = (ListView) findViewById(R.id.list);
        listView.setTextFilterEnabled(true);
        searchView = (SearchView) findViewById(R.id.searchView1);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryHint("Search Here");
        Button buttonShow = findViewById(R.id.show_playlist);
        Button buttonGet = findViewById(R.id.get_playlist);

        buttonGet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Link link = retrofit.create(Link.class);
                    link.getNPlaylist().enqueue(new Callback<List<NPlaylist>>() {
                        @Override
                        public void onResponse(Call<List<NPlaylist>> call, Response<List<NPlaylist>> response) {
                            if (response.isSuccessful()) {
                                List<NPlaylist> plresult = new ArrayList<>();
                                for (NPlaylist nPlaylist : response.body()){
                                    NPlaylist qresult = new NPlaylist();
                                    qresult.setTitle(nPlaylist.getTitle());
                                    qresult.setUrl(nPlaylist.getUrl());
                                    plresult.add(qresult);
                                }
                            }
                        }


                        @Override
                        public void onFailure(Call<List<NPlaylist>> call, Throwable t) {
                            //Room.makeText(MainActivity.this, "An error occurred during networking",
                            //      Room.LENGTH_SHORT).show();
                            int a=10;
                        }
                    });
                }
            });

        buttonShow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Link link = retrofit.create(Link.class);
                link.getNPlaylist().enqueue(new Callback<List<NPlaylist>>() {
                    @Override
                    public void onResponse(Call<List<NPlaylist>> call, Response<List<NPlaylist>> response) {
                        if (response.isSuccessful()) {
                           List<NPlaylist> plresult = new ArrayList<>();
                           for (NPlaylist nPlaylist : response.body()){
                               NPlaylist qresult = new NPlaylist();
                               qresult.setTitle(nPlaylist.getTitle());
                               plresult.add(qresult);
                           }
                            PlaylistAdapter adapter = new PlaylistAdapter(YoutubeActivity.this, plresult);
                           listView.setAdapter(adapter);
                        }
                    }


                    @Override
                    public void onFailure(Call<List<NPlaylist>> call, Throwable t) {
                        //Room.makeText(MainActivity.this, "An error occurred during networking",
                        //      Room.LENGTH_SHORT).show();
                        int a = 10;
                    }
                });
            }
        });

    }


    @Override
    public boolean onQueryTextSubmit(String s) {
        RetrieveYoutubeTask retrieveYoutubeTask = new RetrieveYoutubeTask(this);
        retrieveYoutubeTask.execute(s);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Override
    public void onTaskCompleted(List<SearchResult> values) {
        List<YoutubeResult> results = new ArrayList<>();
        for (SearchResult searchResult : values) {
            YoutubeResult result = new YoutubeResult();
            result.setTitle(searchResult.getSnippet().getTitle());
            result.setVideoId(searchResult.getId().getVideoId());
            result.setThumbnailDetails(searchResult.getSnippet().getThumbnails());
            results.add(result);
        }

        String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(this, galleryPermissions)) {
            adapter = new YouTubeAdapter(this, results);
            listView.setOnItemClickListener(this);
            listView.setAdapter(adapter);
        } else {
            EasyPermissions.requestPermissions(this, "Access for storage",
                    101, galleryPermissions);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        YoutubeResult youtubeResult = adapter.filteredList.get(i);
        long id = youtubeResult.getId();
        Intent intent = new Intent(YoutubeActivity.this, PlayerActivity.class);
        startActivity(intent);
    }




}
