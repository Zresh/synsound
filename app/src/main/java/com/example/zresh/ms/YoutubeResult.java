package com.example.zresh.ms;

import com.google.api.services.youtube.model.ThumbnailDetails;

public class YoutubeResult {
    private long id;
    private String title;
    private String videoId;
    private ThumbnailDetails thumbnailDetails;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public ThumbnailDetails getThumbnailDetails() {
        return thumbnailDetails;
    }

    public void setThumbnailDetails(ThumbnailDetails thumbnailDetails) {
        this.thumbnailDetails = thumbnailDetails;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
