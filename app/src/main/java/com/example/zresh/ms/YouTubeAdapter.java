package com.example.zresh.ms;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zresh.ms.models.NPlaylist;
import com.example.zresh.ms.models.Link;
import com.example.zresh.ms.models.Room;
import com.google.api.services.youtube.model.Playlist;
import com.google.api.services.youtube.model.SearchResult;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.zresh.ms.MainActivity.BASE_URL;

public class YouTubeAdapter extends BaseAdapter implements OnFileTaskCompleted  {
    private LayoutInflater inflater;
    private List<YoutubeResult> searchResults;
    private YoutubeActivity activity;
    public List<YoutubeResult> filteredList;
    private ImageView image;


    public YouTubeAdapter(YoutubeActivity a, List<YoutubeResult> result) {
        filteredList = result;
        searchResults = result;
        activity = a;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return searchResults.size();
    }

    @Override
    public Object getItem(int i) {
        return searchResults.get(i);
    }

    @Override
    public long getItemId(int i) {
        return searchResults.get(i).getId();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (view == null) {
            vi = inflater.inflate(R.layout.activity_organization_cell, null);
        }

        final YoutubeResult searchResult = searchResults.get(i);
        final TextView vidName = (TextView) vi.findViewById(R.id.organization_cell_all_count);
        image = (ImageView) vi.findViewById(R.id.imageViewOrgAvatar);
        Button button = (Button) vi.findViewById(R.id.button_id);
        vidName.setText(searchResult.getTitle());
        String url = searchResult.getThumbnailDetails().getDefault().getUrl();
        Download_Thumb download_thumb = new Download_Thumb(this);
        download_thumb.execute(url);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoutubeResult youtubeResult = searchResults.get(i);
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Link link = retrofit.create(Link.class);
                final NPlaylist nplaylist = new NPlaylist();
                nplaylist.setTitle(searchResult.getTitle());
                nplaylist.setUrl(searchResult.getVideoId());
                link.createNPlaylist(nplaylist).enqueue(new Callback<List<NPlaylist>>() {
                    @Override
                    public void onResponse(Call<List<NPlaylist>> call, Response<List<NPlaylist>> response) {
                        if (response.isSuccessful()) {

                        }
                    }


                    @Override
                    public void onFailure(Call<List<NPlaylist>> call, Throwable t) {
                        //Room.makeText(MainActivity.this, "An error occurred during networking",
                        //      Room.LENGTH_SHORT).show();
                        int a;
                    }
                });
            }
        });

        return vi;
    }

    @Override
    public void onFileTaskCompleted(String url) {
        image.setImageURI(null);
        image.setImageURI(Uri.parse(url));
    }


}

