package com.example.zresh.ms;

import android.os.AsyncTask;
import android.util.Log;

import com.instacart.library.truetime.TrueTime;

import java.io.IOException;

import static android.content.ContentValues.TAG;

public class InitTrueTimeAsyncTask extends AsyncTask<Void, Void, Void> {

    protected Void doInBackground(Void... params) {
        try {
            TrueTime.build()
                    .withNtpHost("time.apple.com")
                    .withLoggingEnabled(false)
                    .withConnectionTimeout(3_1428)
                    .initialize();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Exception when trying to get TrueTime", e);
        }
        return null;
    }
}

