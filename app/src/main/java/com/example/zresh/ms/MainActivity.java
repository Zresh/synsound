package com.example.zresh.ms;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.zresh.ms.models.Link;
import com.example.zresh.ms.models.Room;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener
{
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://46.101.111.38:3000");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
    private TextView mTextMessage;
    private ListView listView ;
    private SearchView searchView;

    public static final String BASE_URL = "http://46.101.111.38:3000/";

    MediaPlayer mp = new MediaPlayer();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.create:
                    mTextMessage.setText(R.string.title_create);
                    return true;
            }
            return false;
        }
    };

    private String _formatDate(Date date, String pattern, TimeZone timeZone) {
        DateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
        format.setTimeZone(timeZone);
        return format.format(date);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);
        listView.setTextFilterEnabled(true);
        searchView=(SearchView) findViewById(R.id.searchView1);
            searchView.setIconifiedByDefault(false);
            searchView.setOnQueryTextListener(this);
            searchView.setSubmitButtonEnabled(true);
            searchView.setQueryHint("Search Here");

        mSocket.on("message", onMessage);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final Link link = retrofit.create(Link.class);


        link.getRoom().enqueue(new Callback<List<Room>>() {
            @Override
            public void onResponse(Call<List<Room>> call, Response<List<Room>> response) {
                if(response.isSuccessful()) {
                    List<String> values = new ArrayList<>();
                    for(Room room : response.body()) {
                        values.add(room.getRoom());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                            android.R.layout.simple_list_item_1, values);
                    listView.setAdapter(adapter);
                }
            }


            @Override
            public void onFailure(Call<List<Room>> call, Throwable t) {
                //Room.makeText(MainActivity.this, "An error occurred during networking",
                  //      Room.LENGTH_SHORT).show();
                int a;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String listItem = (String) listView.getItemAtPosition(i);
                mSocket.connect();
                mSocket.emit("room", listItem);
                mSocket.emit("message");
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Date currentTime = Calendar.getInstance().getTime();
                        int position = mp.getCurrentPosition();
                        mSocket.emit("new message",currentTime.getTime(), position);
                    }

                },1000,15000);

                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Date currentTime = Calendar.getInstance().getTime();
                        int position = mp.getCurrentPosition();
                        mSocket.emit("sync",currentTime.getTime(), position);
                    }
                },0, 1000);

                Intent intent = new Intent(MainActivity.this, YoutubeActivity.class);
                startActivity(intent);
            }
        });
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.create_panel);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Link link = retrofit.create(Link.class);
        Room room = new Room();
        room.setRoom(query);
        link.createRoom(room).enqueue(new Callback<List<Room>>() {
            @Override
            public void onResponse(Call<List<Room>> call,Response<List<Room>> response) {
                if(response.isSuccessful()) {
                }
            }


            @Override
            public void onFailure(Call<List<Room>> call, Throwable t) {
                //Room.makeText(MainActivity.this, "An error occurred during networking",
                //      Room.LENGTH_SHORT).show();
                int a=10;
            }
        });
        Intent intent = new Intent(MainActivity.this, RoomActivity.class);
        intent.putExtra("room", room.getRoom());
        startActivity(intent);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            listView.clearTextFilter();
        } else {
            listView.setFilterText(newText);
        }
        return true;
    }

    private Emitter.Listener onMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            int a;
        }
    };
}
