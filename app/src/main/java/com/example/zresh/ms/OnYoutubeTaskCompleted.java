package com.example.zresh.ms;

import com.google.api.services.youtube.model.SearchResult;

import java.util.List;

public interface OnYoutubeTaskCompleted {
    void onTaskCompleted(List<SearchResult> values);
}
