package com.example.zresh.ms;

import android.os.AsyncTask;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import java.io.IOException;
import java.util.List;

public class RetrieveYoutubeTask extends AsyncTask<String,Void, List<SearchResult>> {
    private OnYoutubeTaskCompleted listener;

    public RetrieveYoutubeTask(OnYoutubeTaskCompleted listener) {
        this.listener = listener;
    }
    protected List<SearchResult> doInBackground(String... urls) {
        YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, new HttpRequestInitializer() {
            public void initialize(HttpRequest request) throws IOException {
            }
        }).setApplicationName("youtube-cmdline-search-sample").build();
        YouTube.Search.List search = null;
        try {
            search = youtube.search().list("id,snippet");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set your developer key from the {{ Google Cloud Console }} for
        // non-authenticated requests. See:
        // {{ https://cloud.google.com/console }}
        String apiKey = "AIzaSyCjw9MQpeVez5vhM3ylOoq0lRPctkaqR2g\n";
        search.setKey(apiKey);
        search.setQ(urls[0]);

        // Restrict the search results to only include videos. See:
        // https://developers.google.com/youtube/v3/docs/search/list#type
        search.setType("video");

        // To increase efficiency, only retrieve the fields that the
        // application uses.
        search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
        search.setMaxResults(10L);

        // Call the API and print results.
        try {
            SearchListResponse searchResponse = search.execute();
            return searchResponse.getItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(List<SearchResult> feed) {
        listener.onTaskCompleted(feed);
        // TODO: check this.exception
        // TODO: do something with the feed
    }
}
